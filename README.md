Lysander is script for loading files over telnet where there is a file system or virtual file system on the target machine
but there are limited networking options available. Copies files over telnet without any addition software installed on the client or telnet server.
Drawbacks: It's not super fast. It reads through your text file and sends each character as a BASH variable on the target machine then concatenates them into a line for each line of yoru file.
Also seems to hang. Use with timeout and give enough time to copy all of the text. Slow but you can copy a script or source code file via telnet.

optional arguments:
  -h, --help            show this help message and exit
  --infile INFILE, -i INFILE
                        path to file on local machine
  --outfile OUTFILE, -o OUTFILE
                        path to file remote machine
  --target TARGET, -t TARGET
                        remote host IP address
  --user USER, -u USER  telnet username
  --password PASSWORD, -p PASSWORD
                        telnet password

Example:
timeout 33 python3 lysander.py -i test.sh -o test.sh -t 192.168.0.41 -u root -p password