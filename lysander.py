#!/usr/bin/python3
#script by Buck Roberts http://realbuckroberts.com friend me on social media and add me to your Spotify, Pandora, or whatever playlists
#This script is for sending text files over telnet. It works but I couldn't figure out how to get telnetlib to listen for wildcard prompt and interact properly so I just made it send all of the data. I need some help making this thing actually work the way that it is supposed to.
#What it does is it read each character in each line of a file then writes an environmental variable for BASH on the target machine. The it will concatenate those variables into a single line and echo it a file on the target machine.
#use with timeout for now because I can't figure out how to make telnetlib read for prompt correctly
#timeout 33 python3 telnetecholoader.py -i test.sh -o test.sh -t 192.168.0.41 -u admin -p admin
import getpass
import sys
import telnetlib
import os
import argparse
pwd=os.getcwd()
#parse command line arguement
parser = argparse.ArgumentParser()
parser.add_argument("--infile", "-i", help="path to file on local machine")
parser.add_argument("--outfile", "-o", help="path to file remote machine")
parser.add_argument("--target", "-t", help="remote host IP address")
parser.add_argument("--user", "-u", help="telnet username")
parser.add_argument("--password", "-p", help="telnet password")
args = parser.parse_args()
i=args.infile
o=args.outfile
t=args.target
u=args.user
p=args.password

tn = telnetlib.Telnet(t)

#sleep(10)
#find a wild card statement for telnet
tn.read_until("login: ".encode('utf-8'))

sendu=u+"\n"
senduser=sendu.encode('utf-8')
tn.write(senduser)
#sleep(10) may just sleep between commmands then send statments without reading
tn.read_until("Password: ".encode('utf-8'))
sendp=p+"\n"
sendpass=sendp.encode('utf-8')
tn.write(sendpass)

terminaltype="vt100\n".encode('utf-8')
tn.write(terminaltype)

#begin echoloader code


with open(i) as f:
    echostatement =''
    for line in f:
        line.strip()
        counter=0
        fullline=""
        fullvars=""
        for letter in line.strip():
            if letter == '"':
                letter='\\"'
            elif letter=="'":
                letter="\\'"
            setletter='a'+str(counter)+'=\"'+letter+'\"'
            fullvars=fullvars+setletter+' && '
            #print(setletter)
            #print(fullvars)
            fullline = fullline+'$a'+str(counter)
            counter +=1
        #print(fullvars)
        #print(fullline)
        echostatement = fullvars+'echo '+fullline+' >> '+o
        #print(echostatement)
        #os.system(echostatement)
        tnecho=echostatement+"\n"
        techno=tnecho.encode('utf-8')
        tn.write(techno)

##set telnet to character mode in order to send big lines          
f.close()

print(tn.read_all())
#end echoloader code
tn.write("quit\n".encode('utf-8'))
tn.close()